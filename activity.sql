-- 1. Return the customerName of the customers who are from the Philippines.

SELECT customerName FROM customers WHERE country ="Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".

SELECT contactLastName, contactFirstName FROM customers WHERE customerName="La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic".

SELECT productName, MSRP FROM products WHERE productName="The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com".

SELECT lastName, firstName FROM employees WHERE email="jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state.

SELECT customerName FROM customers WHERE state is NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.

SELECT lastName, firstName, email FROM employees WHERE lastName ="Patterson" AND firstName = "Steve";

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'.

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'.

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers without duplication.

SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of orders without duplication.

SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada.

SELECT customerName, country FROM customers WHERE country ="USA" or country ="France" or country = "Canada";

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo.

SELECT employees.lastName, employees.firstName, offices.city FROM employees JOIN offices WHERE employees.officeCode = 5 AND offices.officeCode = 5;

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson".

SELECT customerName FROM customers JOIN employees WHERE employees.employeeNumber = customers.salesRepEmployeeNumber AND employees.lastName = "Thompson" AND employees.firstName="Leslie";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports".

SELECT customerName, productName FROM customers JOIN products, orders, orderDetails WHERE orders.orderNumber = orderDetails.orderNumber AND orderDetails.productCode = products.productCode AND customers.customerNumber = orders.customerNumber AND customers.customerName = "Baane Mini Imports";

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country.

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees JOIN customers ,offices WHERE employees.officeCode = offices.officeCode AND customers.country = offices.country;

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.

SELECT productName, quantityInStock FROM products WHERE productLIne = "Planes" AND quantityInStock < 1000;

-- 18. Return the customer's name with a phone number containing "+81".

SELECT customerName,phone FROM customers WHERE phone LIKE "%+81%"